package ru.t1consulting.nkolesnik.tm.exception.field;

public final class EmailAlreadyExistException extends AbstractFieldException {

    public EmailAlreadyExistException() {
        super("Error! Email already exist...");
    }

}
