package ru.t1consulting.nkolesnik.tm.api.repository;

import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    boolean existsById(String id);

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    List<M> findAll(Sort sort);

    M findByIndex(Integer index);

    M findById(String id);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    void clear();

    int getSize();
    
}
