package ru.t1consulting.nkolesnik.tm.command.user;

import ru.t1consulting.nkolesnik.tm.api.service.IAuthService;
import ru.t1consulting.nkolesnik.tm.api.service.IUserService;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;
import ru.t1consulting.nkolesnik.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    protected void showUser(final User user) {
        System.out.println("ID: " + user.getId());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

}
