package ru.t1consulting.nkolesnik.tm.command;

import ru.t1consulting.nkolesnik.tm.api.model.ICommand;
import ru.t1consulting.nkolesnik.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract String getName();

    public abstract String getDescription();

    public String getArgument() {
        return null;
    }

    public abstract void execute();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        String result = "";
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        if (name != null && !name.isEmpty()) {
            result += name + " : ";
        }
        if (argument != null && !argument.isEmpty()) {
            result += argument + " : ";
        }
        if (description != null && !description.isEmpty()) {
            result += description;
        }
        return result;
    }
}
