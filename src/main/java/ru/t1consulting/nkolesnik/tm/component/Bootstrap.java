package ru.t1consulting.nkolesnik.tm.component;

import ru.t1consulting.nkolesnik.tm.api.repository.ICommandRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.api.service.*;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;
import ru.t1consulting.nkolesnik.tm.command.project.*;
import ru.t1consulting.nkolesnik.tm.command.system.*;
import ru.t1consulting.nkolesnik.tm.command.task.*;
import ru.t1consulting.nkolesnik.tm.command.user.*;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.system.ArgumentNotSupportedException;
import ru.t1consulting.nkolesnik.tm.exception.system.CommandNotSupportedException;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.repository.CommandRepository;
import ru.t1consulting.nkolesnik.tm.repository.ProjectRepository;
import ru.t1consulting.nkolesnik.tm.repository.TaskRepository;
import ru.t1consulting.nkolesnik.tm.repository.UserRepository;
import ru.t1consulting.nkolesnik.tm.service.*;
import ru.t1consulting.nkolesnik.tm.util.DateUtil;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);


    {
        registry(new InfoCommand());
        registry(new AboutCommand());
        registry(new VersionCommand());
        registry(new HelpCommand());
        registry(new CommandsCommand());
        registry(new ArgumentsCommand());
        registry(new ExitCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIProjectIdCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new UserRegistryCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserViewProfileCommand());
        registry(new UserUpdateProfileCommand());
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String[] args) {
        if (processArgument(args)) {
            new ExitCommand().execute();
        }
        initData();
        initUser();
        initLogger();
        while (true) {
            try {
                System.out.println("Enter command:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void initData() {
        taskService.add(new Task("DEMO TASK 1", Status.NOT_STARTED, DateUtil.toDate("01.01.2001")));
        taskService.add(new Task("DEMO TASK 2", Status.IN_PROGRESS, DateUtil.toDate("02.02.2002")));
        taskService.add(new Task("DEMO TASK 3", Status.COMPLETED, DateUtil.toDate("03.03.2003")));
        projectService.add(new Project("DEMO PROJECT 1", Status.NOT_STARTED, DateUtil.toDate("05.04.2004")));
        projectService.add(new Project("DEMO PROJECT 2", Status.NOT_STARTED, DateUtil.toDate("04.04.2004")));
        projectService.add(new Project("DEMO PROJECT 3", Status.NOT_STARTED, DateUtil.toDate("06.06.2004")));
        Project project = new Project("PROJECT 1", Status.NOT_STARTED, DateUtil.toDate("09.09.2009"));
        taskService.create("Task To Project").setProjectId(project.getId());
        projectService.add(project);
    }

    private void initUser() {
        userService.create("test", "test", "test@test.email.ru", Role.USUAL);
        userService.create("admin", "admin", "admin@admin.email.ru", Role.ADMIN);
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    private boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) {
            return false;
        }
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}


