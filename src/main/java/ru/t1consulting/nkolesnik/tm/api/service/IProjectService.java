package ru.t1consulting.nkolesnik.tm.api.service;

import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IService<Project> {

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusById(String id, Status status);

}
