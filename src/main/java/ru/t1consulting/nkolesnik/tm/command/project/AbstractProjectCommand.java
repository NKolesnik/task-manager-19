package ru.t1consulting.nkolesnik.tm.command.project;

import ru.t1consulting.nkolesnik.tm.api.service.IProjectService;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectTaskService;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.util.DateUtil;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    public IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    protected void showProject(final Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

}
